﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8_CSharp_Day5
{
    /*
     Develop a CustomQueue class with methods for Enqueue, Dequeue, Peek, and IsEmpty.
     Show how your queue can handle different data types by enqueuing strings 
        and integers, then dequeuing and displaying them to confirm FIFO order.
     */

    public class CustomQueue
    { 
        static Queue<Object> Queue = new Queue<Object>();

        public bool IsEmpty()
        {
            if (Queue.Count == 0)
                return false;
            else
                return true;
        }
        public void Enqueue()
        {
            Console.WriteLine("Enter item to insert:");
            Object obj = Console.ReadLine();
            Queue.Enqueue(obj);
            Console.WriteLine(obj + " Inserted Successfully .");
        }

        public void Dequeue()
        {
            bool check = IsEmpty();
            if (check == true)
            {
                Object x = Queue.Dequeue();
                Console.WriteLine(x + " is Removed.");
            }
            else
            {
                Console.WriteLine("queue is Empty !");
            }
        }

        public void peek()
        {
            Console.WriteLine("First item is:" + Queue.Peek());
        }
        public void display()
        {
            foreach (var item in Queue)
            {
                Console.WriteLine(item);
            }
        }

    }

    class task2_2
    {
        static void Main()
        {
            CustomQueue stack = new CustomQueue();
            int ch;
            do
            {
                Console.WriteLine("Enter Your Choice:\t 1.Enqueue 2.Dequeue 3.Peek  4.Display 0.Exit==>");
                ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1: stack.Enqueue(); break;
                    case 2: stack.Dequeue(); break;
                    case 3: stack.peek(); break;
                    case 4: stack.display(); break;
                    case 0: break;
                    default:
                        Console.WriteLine("Invalid Choice !!");
                        break;
                }
            }
            while (ch != 0);
        }
    }
}
