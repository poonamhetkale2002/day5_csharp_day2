﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8_CSharp_Day5
{
    /*
     1) Create a CustomStack class in C# with operations Push, Pop, Peek, and IsEmpty.
        Demonstrate its LIFO behavior by pushing integers onto the stack, then popping 
        and displaying them until the stack is empty.
     */

    public class CustomStack
    {
         static Stack<int> stack=new Stack<int>();

        public bool IsEmpty()
        {
            if (stack.Count == 0)
                return false;
            else
                return true;
        }
        public void push()
        {            
            Console.WriteLine("Enter Item to Push:");
            int item=Convert.ToInt32(Console.ReadLine());
            stack.Push(item);
            Console.WriteLine(item+" Inserted Successfully .");
        }

        public void pop()
        {
            bool check=IsEmpty();
            if (check==true)
            {
                int x = stack.Pop();
                Console.WriteLine(x + " is Removed.");
            }
            else
            {
                Console.WriteLine("Stack is Empty !");
            }
        }
        public void peek()
        {
            Console.WriteLine("TopMost item is:"+stack.Peek());
        }
        public void Display()
        {
            foreach (var item in stack)
                Console.WriteLine(item);
        }
        
    }

    class task2_1
    {
        static void Main()
        {
            CustomStack stack = new CustomStack();
            int ch;
            do
            {
                Console.WriteLine("Enter Your Choice:\t 1.Push 2.Pop 3.Peek  4.Display 0.Exit==>");
                ch=Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:stack.push(); break;
                    case 2:stack.pop(); break;
                    case 3:stack.peek(); break;
                    case 4:stack.Display(); break;
                    case 0: break;
                    default: Console.WriteLine("Invalid Choice !!");
                        break;
                }
            }
            while (ch != 0);
        }
    }
}
