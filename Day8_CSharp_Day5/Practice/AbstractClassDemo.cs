﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    abstract class figure
    {
        protected float dimension1, dimension2, area;
        public abstract void GetValues();
        public abstract void CalculateArea();
        public void DisplayArea()
        {
            Console.WriteLine("Area is " + area);
        }
    }
    class square : figure
    {

        public override void GetValues()
        {
            Console.WriteLine("Enter Side");
            dimension1 = float.Parse(Console.ReadLine());
        }
        public override void CalculateArea()
        {
            area = dimension1 * dimension1;

        }
    }


    class rectangle : figure

    {

        public override void GetValues()
        {
            Console.WriteLine("Enter Length of rectangle");
            dimension1 = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter Breadth of rectangle");
            dimension2 = float.Parse(Console.ReadLine());
        }
        public override void CalculateArea()
        {
            area = dimension1 * dimension2;

        }

    }

    class triangle : figure

    {

        public override void GetValues()
        {
            Console.WriteLine("Enter Length of triangle");
            dimension1 = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter Breadth of triangle");
            dimension2 = float.Parse(Console.ReadLine());
        }
        public override void CalculateArea()
        {
            area = dimension1 * dimension2;

        }

    }

    class AbstractClassDemo
    {
        static void Main()
        {
            square square = new square();
            square.GetValues();
            square.CalculateArea();
            square.DisplayArea();

            rectangle rectangle = new rectangle();
            rectangle.GetValues();
            rectangle.CalculateArea();
            rectangle.DisplayArea();

            triangle triangle = new triangle();
            triangle.GetValues();
            triangle.CalculateArea();
            triangle.DisplayArea();

        }
    }
}
