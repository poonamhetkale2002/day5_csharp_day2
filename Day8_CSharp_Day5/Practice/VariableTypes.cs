﻿namespace Practice
{
    public class Student
    {
        readonly public int id;
        string name;
        static string batch = "B001";
        public const string companyName = "Wipro";
        int marks;
        public  void batchDetails()
        {
            batch = "C# Batch2";
            Console.WriteLine("Batch Name:" + batch);
        }
        public Student()
        {
            Console.WriteLine("Enter your id:");
            id=Convert.ToInt32(Console.ReadLine());
        }
        public void getDetails()
        {
            Console.WriteLine("Enter your name:");
            name = Console.ReadLine();
            Console.WriteLine("Enter your Marks:");
            marks = Convert.ToInt32(Console.ReadLine());
        }
        public void displayDetails()
        {
            Console.WriteLine("Your Name:" + name);
            Console.WriteLine("Your id:" + id);
           // Console.WriteLine("Your BAtch:" + batch);
            Console.WriteLine("Your marks:" + marks);
            Console.WriteLine("Your Company name:"+companyName);
        }
}
class VariableTypes
{
    static void Main(string[] args)
        {
           Student student = new Student();
            student.getDetails();
            student.displayDetails();
            student.batchDetails();
            Console.ReadLine();
        }
    }
}
