﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    struct A
    {
        public int a;
    }
    struct B
    {
        public int b;
    }
    class Addition
    {
        A x;
        B y;
        public void getDetails()
        {          
            Console.WriteLine("Enter value of a:") ;
            x.a=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter value of b:");
            y.b = Convert.ToInt32(Console.ReadLine());
        }
        public void add()
        {
            Console.WriteLine("Addition:" +(x.a+y.b));
        }
    }
    class MultipleStructure
    {
        static void Main()
        {
            Addition addition = new Addition();
            addition.getDetails();
            addition.add();
            Console.ReadLine();
        }
    }
}
